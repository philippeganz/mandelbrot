var searchData=
[
  ['main',['main',['../main_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main.cpp']]],
  ['main_2ecpp',['main.cpp',['../main_8cpp.html',1,'']]],
  ['mandelbrot2',['mandelbrot2',['../class_complex.html#a09c885a08a45b83b730eee74b3cede2b',1,'Complex']]],
  ['mandelbrotpow',['mandelbrotPow',['../class_complex.html#ae62edf7b65415e50815072d23171eb8e',1,'Complex']]],
  ['master',['master',['../namespacedistributed.html#aabc1c1572f423a374352fca592356ccf',1,'distributed']]],
  ['master_2ecpp',['master.cpp',['../master_8cpp.html',1,'']]],
  ['multithread',['multithread',['../namespacelocal.html#a191e319775663a25cbf49fd6169a0822',1,'local']]],
  ['multithread_2ecpp',['multithread.cpp',['../multithread_8cpp.html',1,'']]]
];
