/// \file src/main.cpp
/// \brief Launcher for the Mandelbrot computation.
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///

#include "print.hpp"
#include "local.hpp"
#include "distributed.hpp"

#include <chrono>

#include <mpi.h>
#include <omp.h>

int main( int argc, char **argv )
{
#if PRECISION == FLOAT32
    float real_midpoint{}, real_startpoint{}, imag_midpoint{}, imag_startpoint{};
#elif PRECISION == FLOAT64
    double real_midpoint{}, real_startpoint{}, imag_midpoint{}, imag_startpoint{};
#elif PRECISION == FLOAT80
    long double real_midpoint{}, real_startpoint{}, imag_midpoint{}, imag_startpoint{};
#elif PRECISION == FLOAT128
    __float128 real_midpoint{}, real_startpoint{}, imag_midpoint{}, imag_startpoint{};
#else
    #error ERROR_MESSAGE
#endif // PRECISION
    size_t width{};
    size_t height{};
    double resol{};
    size_t max_iter{};
    unsigned int** result = nullptr;

    int myRank{};
    int nProc{};
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &myRank );
    MPI_Comm_size( MPI_COMM_WORLD, &nProc );

    // Local
    if( nProc == 1)
    {
        bool fail = false;
        if( argc != 8 )
        {
            fail = true;
        }

        std::string localType;

        if( !fail )
        {
            localType = argv[1];
            if( localType.compare("seq") != 0 && localType.compare("omp") != 0 )
            {
                fail = true;
            }

#if PRECISION == FLOAT32
            real_midpoint = strtof( argv[2], nullptr );
            imag_midpoint = strtof( argv[3], nullptr );
#elif PRECISION == FLOAT64
            real_midpoint = strtod( argv[2], nullptr );
            imag_midpoint = strtod( argv[3], nullptr );
#elif PRECISION == FLOAT80
            real_midpoint = strtold( argv[2], nullptr );
            imag_midpoint = strtold( argv[3], nullptr );
#elif PRECISION == FLOAT128
            real_midpoint = strtoflt128( argv[2], nullptr );
            imag_midpoint = strtoflt128( argv[3], nullptr );
#else
    #error ERROR_MESSAGE
#endif // PRECISION
            width = strtoul( argv[4], nullptr, 0 );
            height = strtoul( argv[5], nullptr, 0 );
            resol = strtod( argv[6], nullptr );
            max_iter = strtoul( argv[7], nullptr, 0 );

            real_startpoint = real_midpoint - resol*width/2;
            imag_startpoint = imag_midpoint - resol*height/2;
        }

        if( !fail )
        {
            try
            {
                std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();

                if( localType.compare("seq") == 0 )
                {
                    std::cout << "single thread computation" << std::endl;
                    std::cout << "width = " << width << ", height = " << height;
                    std::cout  << ", iter = " << max_iter;
                    result = local::sequential( real_startpoint, imag_startpoint, width, height, resol, max_iter );
                }
                else
                {
                    std::cout << omp_get_max_threads() << " threads computation" << std::endl;
                    std::cout << "width = " << width << ", height = " << height;
                    std::cout  << ", iter = " << max_iter;
                    result = local::multithread( real_startpoint, imag_startpoint, width, height, resol, max_iter );
                }

                std::chrono::duration<double> elapsed_seconds = std::chrono::high_resolution_clock::now() - start;
                std::cout << ", time = " << elapsed_seconds.count() << std::endl;
            }
            catch( const std::string error )
            {
                std::cerr << error << std::endl;
            }
        }

        if( fail )
        {
            std::cerr << "Local usage : mandelbrot <seq|omp> <real mid-point> <imaginary mid-point> <width> <height> <resolution> <max iterations>" << std::endl;
            MPI_Finalize();
            return EXIT_FAILURE;
        }
    }

    // Distributed
    else
    {
        bool fail = false;
        if( argc != 7 )
        {
            fail = true;
        }

        if( !fail )
        {
            real_midpoint = strtold( argv[1], nullptr );
            imag_midpoint = strtold( argv[2], nullptr );
            width = strtoul( argv[3], nullptr, 0 );
            height = strtoul( argv[4], nullptr, 0 );
            resol = strtod( argv[5], nullptr );
            max_iter = strtoul( argv[6], nullptr, 0 );

            real_startpoint = real_midpoint - resol*width/2;
            imag_startpoint = imag_midpoint - resol*height/2;

            // Master
            if( myRank == 0 )
            {
                std::cout << nProc << " processes computation" << std::endl;
                std::cout << "width = " << width << ", height = " << height;
                std::cout  << ", iter = " << max_iter;

                std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();

                try
                {
                    result = distributed::master( width, height, nProc );
                    std::chrono::duration<double> elapsed_seconds = std::chrono::high_resolution_clock::now() - start;
                    std::cout << ", time = " << elapsed_seconds.count() << std::endl;
                }
                catch( const std::string error )
                {
                    std::cerr << error << std::endl;
                }
            }

            // Slave
            else
            {
                distributed::slave( real_startpoint, imag_startpoint, width, height, resol, max_iter, myRank );
            }
        }

        if( fail )
        {
            std::cerr << "Distributed usage : mandelbrot <real mid-point> <imaginary mid-point> <width> <height> <resolution> <max iterations>" << std::endl;
            MPI_Finalize();
            return EXIT_FAILURE;
        }
    }

    if( myRank == 0 )
    {
        std::string filename = print::filename(real_midpoint, imag_midpoint, width, height, max_iter, resol);
//        print::console( result, filename, width, height, resol );
//        print::raw( result, filename, width, height );
//        print::bw( result, filename, width, height, max_iter );
        print::col( result, filename, width, height, max_iter );

        if( result != nullptr )
        {
            for( size_t i = 0; i < height; ++i )
            {
                delete result[i];
            }
        }
        delete result;
    }


    MPI_Finalize();
}
