/// \file src/convert.cpp
/// \brief Convert a file into another type
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#include "print.hpp"

void print::convert(std::string filename,
                    std::string base_type,
                    std::string target_type )
{
    std::stringstream convert;

#ifdef __linux
    convert << "convert ";
#elif _WIN32
    convert << "magick convert ";
#endif

    convert << filename << "." << base_type << " " << filename << "." << target_type;

    int status = system(convert.str().c_str());
    if( status < 0 )
        std::cerr << filename << "." << base_type << " could not be converted to " << target_type << ", please use the raw file to view your results." << std::endl;
    else
    {
        {
            std::cout << filename << "." << base_type << " successfully converted to " << target_type << " format." << std::endl;
            convert.str(std::string());
#ifdef __linux
            convert << "rm ";
#elif _WIN32
            convert << "del ";
#endif
            convert << filename << ".ppm";
            status = system(convert.str().c_str());
        }
    }
}
