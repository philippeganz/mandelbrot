/// \file src/console.cpp
/// \brief Print to the console
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#include "print.hpp"

void print::console(unsigned int** const &tab,
                    float real_startpoint,
                    float imag_startpoint,
                    size_t width,
                    size_t height,
                    double resol )
{
    std::cout << "     ";
    for( size_t col = 0; col < width; ++col )
    {
        std::cout << std::setw(4) << std::fixed << std::setprecision(1) << real_startpoint + col*resol;
    }
    std::cout << std::endl;
    std::cout << "    " << std::string( width*4 + 3, '-' ) << std::endl;
    for( size_t row = 0; row < height; ++row )
    {
        std::cout << std::setw(4) << imag_startpoint + row*resol << "| ";
        for( size_t col = 0; col < width; ++col )
        {
            std::cout << std::setw(3) << tab[row][col] << " ";
        }
        std::cout << "|" << std::endl;
    }
    std::cout << "    " << std::string( width*4 + 3, '-' ) << std::endl;
}

