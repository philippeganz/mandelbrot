/// \file src/raw.cpp
/// \brief Print as ASCII file
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///

#include "print.hpp"

void print::raw(unsigned int** const &tab,
                std::string filename,
                size_t width,
                size_t height )
{
    std::ofstream file( filename );

    for( size_t row = 0; row < height; ++row )
    {
        for( size_t col = 0; col < width; ++col )
        {
            file << tab[row][col] << " ";
        }

        file << std::endl;
    }
    std::cout << filename << " successfully written on the disk." << std::endl;
}


