/// \file src/bw.cpp
/// \brief Print as grey-scale pgm file
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///

#include "const.hpp"
#include "print.hpp"

void print::bw(unsigned int** const &tab,
               std::string filename,
               size_t width,
               size_t height,
               size_t max_iter )
{
    std::ofstream file( filename + std::string(".pgm") );

    file << "P2" << std::endl << width << " " << height << std::endl << max_iter << std::endl;
    for( size_t row = 0; row < height; ++row )
    {
        for( size_t col = 0; col < width; ++col )
        {
            file << tab[row][col] << " ";
        }
        file << std::endl;
    }
    file.close();

#ifdef CONVERT
    print::convert(filename, std::string("pgm"), std::string("png"));
#endif // CONVERT
}
