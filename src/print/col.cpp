/// \file src/col.cpp
/// \brief Print as RGB ppm file
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///

#include "const.hpp"
#include "print.hpp"

void print::col(unsigned int** const &tab,
                std::string filename,
                size_t width,
                size_t height,
                size_t max_iter )
{
    ++max_iter;
    unsigned int* colorMap[3];
    for( size_t i = 0; i < 3; ++i)
        colorMap[i] = new unsigned int[max_iter];
    float ratio = 4.0/(float)max_iter;
    size_t i = 0;
    // from black to purple
    for( size_t j = 0; j < max_iter/4; ++j, ++i)
    {
        colorMap[0][i] = (unsigned int)(  0 * (1 - j*ratio) + 148 * j * ratio); // R
        colorMap[1][i] =   0;                                                   // G
        colorMap[2][i] = (unsigned int)(  0 * (1 - j*ratio) + 211 * j * ratio); // B
    }
    // from purple to red
    for( size_t j = 0; j < max_iter/4; ++j, ++i)
    {
        colorMap[0][i] = (unsigned int)(148 * (1 - j*ratio) + 255 * j * ratio); // R
        colorMap[1][i] =   0;                                                   // G
        colorMap[2][i] = (unsigned int)(211 * (1 - j*ratio) +   0 * j * ratio); // B
    }
    // from red to orange
    for( size_t j = 0; j < max_iter/4; ++j, ++i)
    {
        colorMap[0][i] = 255;                                                   // R
        colorMap[1][i] = (unsigned int)(  0 * (1 - j*ratio) + 127 * j * ratio); // G
        colorMap[2][i] =   0;                                                   // B
    }
    // from orange to yellow
    for( size_t j = 0; j < max_iter/4; ++j, ++i)
    {
        colorMap[0][i] = 255;                                                   // R
        colorMap[1][i] = (unsigned int)(127 * (1 - j*ratio) + 255 * j * ratio); // G
        colorMap[2][i] =   0;                                                   // B
    }

    std::ofstream file( filename + std::string(".ppm") );

    file << "P3" << std::endl << width << " " << height << std::endl << 255 << std::endl;
    for( size_t row = 0; row < height; ++row )
    {
        for( size_t col = 0; col < width; ++col )
        {
            unsigned int value = tab[row][col];
            file << std::setw(3) << colorMap[0][value] << " " << std::setw(3) << colorMap[1][value] << " " << std::setw(3) << colorMap[2][value] << "  ";
        }
        file << std::endl;
    }
    file.close();

#ifdef CONVERT
    print::convert(filename, std::string("ppm"), std::string("png"));
#endif // CONVERT
}
