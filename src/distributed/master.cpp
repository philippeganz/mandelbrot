/// \file src/distributed/master.cpp
/// \brief Master function of the distributed MPI computation mechanism
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#include "distributed.hpp"

unsigned int** distributed::master(size_t width,
                                   size_t height,
                                   size_t nProc )
{
    MPI_Request* request = new MPI_Request[nProc];
    size_t currentRow = nProc - 1;
    unsigned int** result = new unsigned int*[height];
    unsigned int** recvBuffer = new unsigned int*[nProc];
    bool interrupt = false;
    try
    {
        for( size_t i = 0; i < height; ++i )
            result[i] = new unsigned int[width];
    }
    catch ( std::bad_alloc& no_memory )
    {
        interrupt = true;
        for( size_t i = 0; i < height; ++i )
        {
            delete result[i];
        }
        std::cerr << std::endl << "Could not allocate memory for result array in distributed::master. Requested ";
        std::cerr << std::to_string(height*width*sizeof(unsigned int)/1000000);
        std::cerr << " MB!" << std::endl;
    }
    try
    {
        for( size_t i = 0; i < nProc; ++i )
            recvBuffer[i] = new unsigned int[width + 1];
    }
    catch ( std::bad_alloc& no_memory )
    {
        interrupt = true;
        for( size_t i = 0; i < nProc; ++i )
        {
            delete recvBuffer[i];
        }
        std::cerr << std::endl << "Could not allocate memory for receive buffer in distributed::master. Requested ";
        std::cerr << std::to_string((unsigned long long)nProc*(unsigned long long)(width+1)*(unsigned long long)sizeof(unsigned int)/1000000);
        std::cerr << " MB!" << std::endl;
    }

    if( interrupt )
    {
        unsigned int* tmpRecvBuffer = new unsigned int[width + 1];
        for( size_t proc = 1; proc < nProc; proc++ )
        {
            MPI_Irecv( &tmpRecvBuffer[0], width + 1, MPI_UNSIGNED, proc,
                       MPI_ANY_TAG, MPI_COMM_WORLD, &request[proc] );
            MPI_Wait( &request[proc], MPI_STATUSES_IGNORE );
            MPI_Send( &height, 1, MPI_UNSIGNED, proc, 0, MPI_COMM_WORLD );
        }
        delete tmpRecvBuffer;
        throw std::string("\nCould not allocate enough memory!");
    }

    int done = 0;

    for( size_t proc = 1; proc < nProc; ++proc )
    {
        MPI_Irecv( recvBuffer[proc], width + 1, MPI_UNSIGNED, proc,
                   MPI_ANY_TAG, MPI_COMM_WORLD, &request[proc] );
    }

#ifdef VERBOSE
    std::cout << std::endl;
#endif // VERBOSE

    while( currentRow < height )
    {
        for( size_t proc = 1; proc < nProc; proc++ )
        {
            MPI_Test( &request[proc], &done, MPI_STATUS_IGNORE );

            if( done && currentRow < height )
            {
#ifdef VERBOSE
                std::cout << currentRow << " ";
#endif // VERBOSE
                MPI_Send( &currentRow, 1, MPI_UNSIGNED, proc, 0, MPI_COMM_WORLD );
                currentRow++;
                std::copy(  recvBuffer[proc] + 1, recvBuffer[proc]+ width + 1,
                            result[recvBuffer[proc][0]] );
                MPI_Irecv(  recvBuffer[proc], width + 1, MPI_UNSIGNED, proc,
                            MPI_ANY_TAG, MPI_COMM_WORLD, &request[proc] );
            }
        }
    }

    for( size_t proc = 1; proc < nProc; ++proc )
    {
        MPI_Wait( &request[proc], MPI_STATUSES_IGNORE );
        std::copy(  recvBuffer[proc] + 1, recvBuffer[proc] + width + 1,
                    result[recvBuffer[proc][0]] );
    }

    for( size_t proc = 1; proc < nProc; ++proc )
    {
        MPI_Send( &height, 1, MPI_UNSIGNED, proc, 0, MPI_COMM_WORLD );
    }

    return result;
}
