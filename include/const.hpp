/// \file include/const.hpp
/// \brief Defines used everywhere in the project
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#ifndef CONST_HPP
#define CONST_HPP

// To convert the pgm or ppm file to png after computation.
#define CONVERT

// To be more chatty about what the program is doing.
//#define VERBOSE

// Defines the precision for all the computations. Higher precision increases compute time.
//      float       => 32 bits floating point  ~= 7  digits mantissa
#define FLOAT32 0
//      double      => 64 bits floating point  ~= 15 digits mantissa
#define FLOAT64 1
//      long double => 80 bits floating point  ~= 19 digits mantissa
#define FLOAT80 2
//      __float128  => 128 bits floating point ~= 34 digits mantissa
#define FLOAT128 3

#ifndef PRECISION
    #define PRECISION FLOAT80
#endif // PRECISION

#define ERROR_MESSAGE "PRECISION only supports float, double, long double and __float128 types!"

#endif
