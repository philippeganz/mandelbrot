/// \file include/local.hpp
/// \brief Contains functions to compute locally, possibly multi-threaded.
/// \details Depending on user input, the local computation can be either completely sequential with a
///          single main thread or multi-threaded using the maximum amount of logical cores on the machine.
///          The OpenMP strategy relies on the OMP_SCHEDULE environment variable, to set up the tasks to
///          the different threads; if the variable is not set, the behavior is implementation defined.
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#ifndef LOCALCOMPUTE_HPP
#define LOCALCOMPUTE_HPP

#include "complex.hpp"
#include "const.hpp"

#include <iostream>
#include <string>

namespace local
{
template <class T>
unsigned int** sequential(T real_startpoint,
                          T imag_startpoint,
                          size_t width,
                          size_t height,
                          double resol,
                          size_t max_iter )
{
    unsigned int** result = new unsigned int*[height];
    try
    {
        for( size_t i = 0; i < height; ++i )
            result[i] = new unsigned int[width];
    }
    catch ( const std::bad_alloc& no_memory )
    {
        for( size_t i = 0; i < height; ++i )
            delete result[i];
        std::cerr << std::endl << "Could not allocate memory for result array in local::sequential. Requested ";
        std::cerr << std::to_string(height * width* (size_t)sizeof(unsigned int)/1000000);
        std::cerr << " MB!" << std::endl;
        throw std::string("\nCould not allocate enough memory!");
    }

#ifdef VERBOSE
    std::cout << std::endl;
#endif // VERBOSE
    for( size_t row = 0; row < height; ++row )
    {
#ifdef VERBOSE
        std::cout << row << " ";
#endif // VERBOSE
        T I = imag_startpoint + (T)row * resol;
        for( size_t col = 0; col < width; ++col )
            result[row][col] = Complex( real_startpoint + (T)col * (T)resol, I ).mandelbrot2( max_iter );
    }

    return result;
}


template <class T>
unsigned int** multithread(T real_startpoint,
                           T imag_startpoint,
                           size_t width,
                           size_t height,
                           double resol,
                           size_t max_iter )
{
    unsigned int** result = new unsigned int*[height];
    try
    {
        for( size_t i = 0; i < height; ++i )
            result[i] = new unsigned int[width];
    }
    catch ( const std::bad_alloc& no_memory )
    {
        for( size_t i = 0; i < height; ++i )
            delete result[i];
        std::cerr << std::endl << "Could not allocate memory for result array in local::multithread. Requested ";
        std::cerr << std::to_string(height * width* (size_t)sizeof(unsigned int)/1000000);
        std::cerr << " MB!" << std::endl;
        throw std::string("\nCould not allocate enough memory!");
    }

#ifdef VERBOSE
    std::cout << std::endl;
#endif // VERBOSE

    #pragma omp parallel for schedule(dynamic)
    for( size_t row = 0; row < height; ++row )
    {
#ifdef VERBOSE
        std::cout << row << " ";
#endif // VERBOSE
        T I = imag_startpoint + (T)row * resol;
        for( size_t col = 0; col < width; ++col )
            result[row][col] = Complex( real_startpoint + (T)col * (T)resol, I ).mandelbrot2( max_iter );
    }

    return result;
}

} // namespace local

#endif //LOCALCOMPUTE_HPP
