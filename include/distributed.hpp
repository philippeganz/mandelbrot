/// \file include/distributed.hpp
/// \brief Contains functions to compute on a parallel distributed system.
/// \details Depending on user input, the computation will go on with the number of process specified.
///          Process 0 is taken as the Master and gives the work to the other process ids who are therefor Slaves.
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-11
/// \copyright GPL-3.0
///

#ifndef DISTRIBUTEDCOMPUTE_HPP
#define DISTRIBUTEDCOMPUTE_HPP

#include "complex.hpp"
#include "const.hpp"

#include <iostream>

#include <mpi.h>

namespace distributed
{
unsigned int** master(size_t width,
                      size_t height,
                      size_t nProc );


template <class T>
void slave(T real_startpoint,
           T imag_startpoint,
           size_t width,
           size_t height,
           double resol,
           size_t max_iter,
           size_t myRank )
{
    size_t row = myRank - 1;
    unsigned int* localResult = new unsigned int[width + 1];

    do
    {
        localResult[0] = row;
        T I = imag_startpoint + (T)row * resol;

        for( size_t col = 0; col < width; ++col )
        {
            localResult[col + 1] = Complex( real_startpoint + (T)col * (T)resol, I ).mandelbrot2( max_iter );
        }

        MPI_Send( localResult, width + 1, MPI_UNSIGNED, 0, 0, MPI_COMM_WORLD );
        MPI_Recv( &row, 1, MPI_UNSIGNED, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUSES_IGNORE );
    }
    while( row < height );
}
}

#endif //DISTRIBUTEDCOMPUTE_HPP
