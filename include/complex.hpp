/// \file include/complex.hpp
/// \brief Personalized complex class containing the Mandelbrot functions.
/// \details The std::complex class was not adapted to this project, so a similar one with more member function has been created.
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///

#ifndef COMPLEX_HPP
#define COMPLEX_HPP

#include <cmath>

#include <quadmath.h>

/// \class Complex
template <class T>
class Complex
{
private:
    T real; ///< Real part of the complex number.
    T imag; ///< Imaginary part of the complex number.

public:
    Complex() : real( (T) 0.0 ), imag( (T) 0.0 ) {} ///< Default constructor, creates a complex number initialized to (0,0).
    Complex( T re, T im ) : real( re ), imag( im ) {} ///< Set constructor, creates a complex number initialized as \a re and \a im as real and imaginary part respectively.
    Complex( const Complex& other ) : real( other.real ), imag( other.imag ) {} ///< Copy constructor, creates a complex number by copying the other instance.

    /// Default destructor, does nothing special.
    ~Complex() {}

    /// Real part getter, returns the \a real value.
    T getReal()
    {
        return real;
    }
    /// Real part setter, changes the \a real value to val.
    void setReal(T val)
    {
        real = val;
    }
    /// Imaginary part getter, returns the \a imag value.
    T getImag()
    {
        return imag;
    }
    /// Imaginary part setter, changes the \a imag value to val.
    void setImag(T val)
    {
        imag = val;
    }
    /// Addition operator, sums \a real and \a imag couple respectively.
    Complex operator+( const Complex & z )
    {
        return Complex( real + z.real, imag + z.imag );
    }
    /// Multiplication operator, multiplies \a real and \a imag couple respectively according to complex multiplication.
    Complex operator*( const Complex & z )
    {
        return Complex( real*z.real - imag*z.imag, real*z.imag + imag*z.real );
    }

    /// Exponential operator, computes the \a exp's power of the complex number.
    /** To speed up computation for arbitrary exponent, it uses the polar coordinates multiplication. */
    Complex operator^( T exp )
    {
        T phi = atan2( imag, real ) * exp;
        T r = std::pow( norm(), exp );
        return Complex( r * cosl( phi ), r * sinl( phi ) );
    }
    /// Norm function, returns the euclidean norm of the complex number.
    T norm()
    {
        return sqrtl( real * real + imag * imag );
    }
    /// Squared norm function, returns the squared euclidean norm of the complex number.
    T sqNorm()
    {
        return real * real + imag * imag;
    }

    /// Standard second order Mandelbrot function.
    /** Returns the result of the power 2 Mandelbrot function for the \a real and \a imag coordinates for a maximum
        iteration of \a max_iterations. It is here optimized for the standard Mandelbrot z = z^2 + c function without
        calling the multiplication operator. */
    unsigned int mandelbrot2( size_t max_iterations )
    {
        T initR = real;
        T initI = imag;
        T realSquared = real * real;
        T imagSquared = imag * imag;
        unsigned int k = 0;
        while( realSquared + imagSquared <= (T)4.0 && k < max_iterations )
        {
            T realTmp = realSquared - imagSquared + initR;
            imag = (T)2.0 * real * imag + initI;
            real = realTmp;
            ++k;
            realSquared = real*real;
            imagSquared = imag*imag;
        }
        return k;
    }

    /// Arbitrary power Mandelbrot function.
    /** Returns the result of the \a exp's power Mandelbrot function for the \a real and \a imag coordinates for a
        maximum iteration of \a max_iterations. Since doing arbitrary exponent, the power operator must be used here.
        This function should NOT be used for power 2 Mandelbrot since is increases computation time for the same result */
    unsigned int mandelbrotPow( size_t max_iterations, T exponent )
    {
        Complex init( real, imag);
        unsigned int k = 0;
        while( sqNorm() <= (T)4.0 && k < max_iterations )
        {
            *this = ((*this)^exponent ) + init;
            ++k;
        }
        return k;
    }
};

#endif // COMPLEX_HPP
