/// \file include/print.hpp
/// \brief Outputs the result of the computation.
/// \details Many options are available, namely raw output in console or file, or picture output in gray scale or color.
/// \author Philippe Ganz <philippe.ganz@gmail.com> 2016-2018
/// \version 2.1.0
/// \date 2018-05-12
/// \copyright GPL-3.0
///


#ifndef PRINT_HPP
#define PRINT_HPP

#include "const.hpp"

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

#include <quadmath.h>

namespace print
{

template <class T>
std::string filename(T real_midpoint,
                     T imag_midpoint,
                     size_t width,
                     size_t height,
                     size_t max_iter,
                     double resol )
{
    std::stringstream filename;

    filename << "mandelbrot_";

#if PRECISION != FLOAT128
#if PRECISION == FLOAT32
    filename << std::setprecision(8);
#elif PRECISION == FLOAT64
    filename << std::setprecision(16);
#else
    filename << std::setprecision(20);
#endif // PRECISION
    filename << std::fixed;
    filename << real_midpoint << "_";
    filename << imag_midpoint << "_";

#else
    char temp[50]{};

    quadmath_snprintf(temp, 50, "%.35Q", real_midpoint);
    std::cout << std::string(temp);
    filename << temp << "_";

    quadmath_snprintf(temp, 50, "%.35Q", imag_midpoint);
    std::cout << std::string(temp);
    filename << temp << "_";
#endif // PRECISION

    filename << width << "_";
    filename << height << "_";

    filename << std::setprecision(5) << std::scientific;
    filename << resol << "_";

    filename << max_iter;

    return filename.str();
}


void console(unsigned int** const &tab,
             float real_startpoint,
             float imag_startpoint,
             size_t width,
             size_t height,
             double resol );


void raw(unsigned int** const &tab,
         std::string filename,
         size_t width,
         size_t height );


void bw(unsigned int** const &tab,
        std::string filename,
        size_t width,
        size_t height,
        size_t max_iter );


void col(unsigned int** const &tab,
         std::string filename,
         size_t width,
         size_t height,
         size_t max_iter );


void convert(std::string filename,
             std::string base_type,
             std::string target_type );

}

#endif //PRINT_HPP
