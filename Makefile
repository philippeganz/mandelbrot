CC := mpic++
CFLAGS := -std=c++17 -O3 -fopenmp -pedantic -Wall

PROJECTNAME := mandelbrot

SOURCEDIR := src
SOURCES := $(shell find $(SOURCEDIR) -name '*.cpp')
HEADERDIR := include
OBJECTDIR := obj
OBJECTS := $(addprefix $(OBJECTDIR)/,$(SOURCES:%.cpp=%.o))


all: $(PROJECTNAME)

rebuild: clean $(PROJECTNAME)

$(PROJECTNAME): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -fopenmp -lquadmath -o $(PROJECTNAME)

-include $(DEPENDS)

$(OBJECTDIR)/%.o: %.cpp
	mkdir -p $(OBJECTDIR)/$(dir $<) && $(CC) $(CFLAGS) $(CFLAGSOPT) -I $(HEADERDIR) -c $< -o $@

.PHONY: clean

clean:
	rm -rf $(OBJECTDIR) $(PROJECTNAME)
